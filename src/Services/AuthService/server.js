import express from 'express';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import authRouter from './routes/AuthController';
import config from './config';
import db from './db';

const PORT = config.port;
const API_PATH = config.api_path;

let app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

//Define routes.
app.use(API_PATH,authRouter);

app.listen(PORT, () => {
    console.log(`Authorization Service listening on port ${PORT}`)
});