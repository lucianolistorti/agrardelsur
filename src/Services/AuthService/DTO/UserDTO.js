
export default class UserDTO {
    constructor(user){
        this.username = user.username;
        this.password = user.password;
        this.role = user.role;
        this.name = user.name;
        this.lastName = user.lastName;
        this.email = user.email;
    }
}