import express from 'express';
import { VerifyToken, VerifyAdminToken } from '../security/TokenValidator';
import LoginService from '../services/LoginService';
import LoginDTO from '../DTO/LoginDTO';
import UserDTO from '../DTO/UserDTO';

let router = express.Router();
let loginService = new LoginService();


router.post('/register', async (req,res) => {

    let userDTO = new UserDTO({
        username: req.body.username,
        role: req.body.role,
        password: req.body.password,
        name: req.body.name,
        lastName: req.body.last_name,
        email: req.body.email
    });
    let savedUser;

    try{
        savedUser = await loginService.register(userDTO);
    }
    catch(err){
        return res.status(400).send({ 
            custom_message: err.custom_message,
            debug_error: err.message 
        });
    } 
    res.status(200).send(savedUser);
});

router.post('/login', async (req, res) => {
    let loginDTO = new LoginDTO(req.body.username,req.body.password);
    let token;

    try {
        token = await loginService.login(loginDTO);
    }
    catch(err) {
        res.status(400).send({
            custom_message: 'Las credenciales de acceso son inválidas.',
            debug_error: err.message 
        });
    }
    res.send({ token });
});


//Example of Authorized endpoint. Use VerifyAdminToken for Admins permissions.
router.get('/secure', VerifyToken, (req,res,next) => {
  res.status(200).send({
    username: req.username, role: req.role
  });
});

export default router;