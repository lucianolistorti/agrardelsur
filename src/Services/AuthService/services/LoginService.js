import User from "../models/User";
import jwt from 'jsonwebtoken';
import config from '../config';

export default class LoginService {
    constructor(){}

    async login(loginDTO){
        try {
            let user = await User.findOne({ username: loginDTO.username }).exec();
            let isMatch = await user.comparePassword(loginDTO.password);

            if(isMatch){
                let tokenData = {
                    userId: user.userId,
                    role: user.role
                }
                let token = jwt.sign(tokenData, config.jwt.key);
                return token; 
            }
            else
                throw new Error('Password is not correct.');
        }
        catch(err){
            throw err;
        }
    }

    async register(userDTO){
        let newUser = new User({
            userId: -1,
            username: userDTO.username,
            role: userDTO.role,
            password: userDTO.password,
            role: userDTO.role,
            name: userDTO.name,
            last_name: userDTO.lastName,
            email: userDTO.email
        });
        let savedUser;

        try {
            let userFound = await User.findOne({ username: newUser.username }).exec();
            if(userFound){
                let err = new Error(`Username '${newUser.username}' was found in database. User id: ${userFound.userId}`);
                err.custom_message = "El usuario ya está en uso.";
                throw err;
            }
            savedUser = await newUser.save();
        }
        catch(err){
            if(!err.custom_message)
                err.custom_message = "Hubo un problema al registrar el usuario.";
            throw err;
        } 
        return savedUser;
    }
}