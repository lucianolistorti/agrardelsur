import mongoose from 'mongoose';

let RoleSchema = new mongoose.Schema({  
    role_id: {
        type: Number,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    }
});

export default mongoose.model('Role', RoleSchema);