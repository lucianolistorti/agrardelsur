import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';
const SALT_WORK_FACTOR = 10;

let UserSchema = new mongoose.Schema({  
    userId: {
        type: Number,
        required: true,
        unique: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    role: {
        type: Number,
        default: 0
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String
    },
    last_name: {
        type: String
    },
    email: {
        type: String, 
        trim: true,
        match: [/.+\@.+\..+/,'']
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});

//CounterSchema for autoincremental user id. Only initialized once.
let CounterSchema = new mongoose.Schema({
    _id: { type: String, required: true},
    seq: { type: Number, default: 0 }
});

let counter = mongoose.model('counter', CounterSchema);

function initialize(){
    let userCounter = new counter({
        _id: 'userId'
    });
    userCounter.save();
}

UserSchema.pre('save', function(next){
  let user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
      if (err) return next(err);

      // hash the password using our new salt
      bcrypt.hash(user.password, salt, async (err, hash) => {
          if (err) return next(err);

          // override the cleartext password with the hashed one and set id.
          user.password = hash;
          if(user.isNew){
            try {
                user.userId = await getIncrementalID(counter);
              }
              catch(err) {
                  next(err);
              }
          }
          next();
      });
  });
});

UserSchema.methods.comparePassword = async function(candidatePassword) {
    return await bcrypt.compare(candidatePassword, this.password);
};

async function getIncrementalID(counter){
    let counterDoc;

    try {
        counterDoc = await counter.findByIdAndUpdate({_id: 'userId'}, {$inc: { seq: 1} }).exec();
    }
    catch(err){
        return err;
    }
    return counterDoc.seq;
}

export default mongoose.model('User', UserSchema);