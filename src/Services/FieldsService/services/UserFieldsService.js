import Field from "../models/Field";
import UserField from "../models/UserField";
import FieldDTO from "../DTO/FieldDTO";

export default class UserFieldsService {
    constructor(){}

    async assignFieldToUser(params){
        let fieldId = params.field_id,
            userId = params.user_id,
            password = params.password;
        let userField;
        
        try {
            let field = await Field.findOne({ field_id: fieldId }).exec();
            if(!field){
                let err = new Error(`Field with id ${fieldId} was not found in database`);
                err.custom_message = 'El lote no fue encontrado.';
                throw err;
            }
            let isMatch = await field.comparePassword(password);
            if(!isMatch){
                let err = new Error(`Field with id ${fieldId} does not match with password`);
                err.custom_message = 'La contraseña es incorrecta.';
                throw err;
            }
            userField = await UserField.findOneAndUpdate({ user_id: userId }, {$set: {user_id: userId}, $addToSet: {fields: fieldId}}, {upsert: true, new: true}).exec();
        }
        catch(err){
            if(!err.custom_message)
                err.custom_message = 'Hubo un problema al buscar el lote.'
            throw err
        }
        return userField;
    }

    async getUserFields(userId){
        let fieldsDTO = [];

        try {
            let fields = (await UserField.findOne({ user_id: userId }).exec()).fields;
            for(let i=0, length=fields.length; i<length; i++){
                let field = await Field.findOne({ field_id: fields[i] }).exec();
                fieldsDTO.push(new FieldDTO({
                    name: field.name,
                    field_id: field.field_id,
                    description: field.description,
                    coordinates: field.coordinates
                }))
            }
            return fieldsDTO;
        }
        catch(err){
            if(!err.custom_message)
                err.custom_message = "Hubo un problema al obtener los lotes.";
            throw err;
        }
    }
} 