import Field from "../models/Field";
import Photo from "../models/Photo";
import FieldDTO from "../DTO/FieldDTO";

export default class LoginService {
    constructor(){}


    async addField(fieldDTO){
        let newField = new Field({
            field_id: -1,
            name: fieldDTO.name,
            password: fieldDTO.password,
            user_id: fieldDTO.user_id,
            description: fieldDTO.description,
            coordinates: fieldDTO.coordinates
        });
        let savedField;

        try {
            let fieldFound = await Field.findOne({ name: newField.name }).exec();
            if(fieldFound){
                let err = new Error(`Field name '${newField.name}' was found in database. Field id: ${fieldFound.field_id}`)
                err.custom_message = "El nombre del Lote ya está en uso.";
                throw err;
            }
            savedField = await newField.save();
        }
        catch(err){
            if(!err.custom_message)
                err.custom_message = "Hubo un problema al guardar el lote.";
            throw err;
        } 
        return savedField;
    }

    async getFields(userId){
        let fieldsDTO = [];

        try {
            let fields = await Field.find({ user_id: userId }).exec();
            for(let i=0, length=fields.length; i<length; i++){
                fieldsDTO.push(new FieldDTO({
                    name: fields[i].name,
                    field_id: fields[i].field_id,
                    description: fields[i].description,
                    coordinates: fields[i].coordinates
                }))
            }
            return fieldsDTO;
        }
        catch(err){
            if(!err.custom_message)
                err.custom_message = "Hubo un problema al obtener los lotes.";
            throw err;
        }
    }

    async addPhotoToField(fieldId){
        let fieldDTO, newPhoto, field, photo;

        try {
            newPhoto = new Photo({photo_id: -1})
            photo = await newPhoto.save();
            field = await Field.findOneAndUpdate({ field_id: fieldId }, { $addToSet: {photos: photo.photo_id} }, {new: true} ).exec();
            fieldDTO = new FieldDTO({
                name: field.name,
                field_id: field.field_id,
                description: field.description,
                photos: field.photos,
                coordinates: field.coordinates
            });
        }
        catch(err){
            if(!err.custom_message)
                err.custom_message = "Hubo un problema al guardar la foto.";
            throw err;
        }
        return fieldDTO;
    }
}