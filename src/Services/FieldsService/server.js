import express from 'express';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import fieldsRouter from './routes/FieldsController';
import userFieldsController from './routes/UserFieldsController';
import config from './config';
import db from './db';

const PORT = config.port;
const USERFIELDS_API_PATH = config.userfields_api_path;
const FIELDS_API_PATH = config.fields_api_path;

let app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

//Define routes.
app.use(FIELDS_API_PATH,fieldsRouter);
app.use(USERFIELDS_API_PATH,userFieldsController);

app.listen(PORT, () => {
    console.log(`Fields Service listening on port ${PORT}`)
});