
export default class FieldDTO {
    constructor(field){
        this.name = field.name;
        this.field_id = field.field_id;
        this.password = field.password;
        this.user_id = field.user_id;
        this.description = field.description;
        this.photos = field.photos;
        this.coordinates = field.coordinates;
    }
}