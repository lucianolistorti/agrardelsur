import express from 'express';
import { VerifyToken, VerifyAdminToken } from '../security/TokenValidator';
import FieldDTO from '../DTO/FieldDTO';
import FieldsService from '../services/FieldsService';

let router = express.Router();
let fieldsService = new FieldsService();

//Add field.
router.post('/', VerifyAdminToken, async (req,res) => {
    let field;
    let fieldDTO = new FieldDTO({
        name: req.body.name,
        password: req.body.password,
        user_id: req.userId,
        description: req.body.description,
        coordinates: req.body.coordinates
    });
    
    try {
        field = await fieldsService.addField(fieldDTO);
    }
    catch(err){
        return res.status(400).send({ 
            custom_message: err.custom_message,
            debug_error: err.message 
        });
    } 
    res.status(200).send(field);
});

//Get all fields created by user.
router.get('/', VerifyToken, async (req,res) => {
    let userId = req.userId;
    let fieldsDTO;
    
    try {
        fieldsDTO = await fieldsService.getFields(userId);
    }
    catch(err){
        return res.status(400).send({ 
            custom_message: err.custom_message,
            debug_error: err.message 
        });
    } 
    res.status(200).send(fieldsDTO);
});

//Add image to a field.
router.post('/:id/photos', VerifyAdminToken, async (req,res) => {
    let fieldId = req.params.id;
    let fieldDTO;
    
    try {
        fieldDTO = await fieldsService.addPhotoToField(fieldId);
    }
    catch(err){
        return res.status(400).send({ 
            custom_message: err.custom_message,
            debug_error: err.message 
        });
    } 
    res.status(200).send(fieldDTO);
});

export default router;