import express from 'express';
import { VerifyToken, VerifyAdminToken } from '../security/TokenValidator';
import UserField from '../models/UserField';
import UserFieldsService from '../services/UserFieldsService';

let router = express.Router();
let userFieldsService = new UserFieldsService();

//Assign field with id to tokens user.
router.post('/', VerifyToken, async (req,res) => {
    let userFields;
    let params = {
        field_id: req.body.field_id,
        password: req.body.password,
        user_id: req.userId
    }
    
    try {
        userFields = await userFieldsService.assignFieldToUser(params);
    }
    catch(err){
        res.status(400).send({ 
            custom_message: err.custom_message,
            debug_error: err.message 
        });
    } 
    res.status(200).send(userFields);
});

//Get all fields from tokens user.
router.get('/', VerifyToken, async (req,res) => {
    let userFields, 
        userId = req.userId;
    
    try {
        userFields = await userFieldsService.getUserFields(userId);
    }
    catch(err){
        return res.status(400).send({ 
            custom_message: err.custom_message,
            debug_error: err.message 
        });
    } 
    res.status(200).send(userFields);
});

export default router;