import mongoose from 'mongoose';

let UserFieldSchema = new mongoose.Schema({  
    user_id: {
        type: Number,
        required: true,
        unique: true
    },
    fields: [{
        type: Number,
        required: true
    }]
});

export default mongoose.model('UserField', UserFieldSchema);