import mongoose from 'mongoose';
import Counter from './Counter';

let PhotoSchema = new mongoose.Schema({  
    photo_id: {
        type: Number,
        required: true,
        unique: true
    },
    uri: {
        type: String,
        default: 'img/no_image.png'
    },
    coordinates: {
        type: String
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});

PhotoSchema.pre('save', async function(next){
    let photo = this;

    if(photo.isNew){
        try {
            photo.photo_id = await getIncrementalID();
        }
        catch(err) {
            next(err);
        }
    }
    next();
});

async function getIncrementalID(){
    let counterDoc;

    try {
        counterDoc = await Counter.findByIdAndUpdate({_id: 'photo_id'}, {$inc: { seq: 1} }).exec();
    }
    catch(err){
        return err;
    }
    return counterDoc.seq;
}

//initialize();
function initialize(){
    let photoCounter = new Counter({
        _id: 'photo_id'
    });
    photoCounter.save();
}

export default mongoose.model('Photo', PhotoSchema);