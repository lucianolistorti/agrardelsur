import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';
import Counter from './Counter';
const SALT_WORK_FACTOR = 10;

let FieldSchema = new mongoose.Schema({  
    field_id: {
        type: Number,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    user_id: {
        type: Number,
        required: true
    },
    description: {
        type: String
    },
    coordinates: {
        type: String
    },
    photos: [{
        type: Number,
        default: []
    }],
    created_date: {
        type: Date,
        default: Date.now
    }
});

FieldSchema.pre('save', function(next){
    let field = this;

    // only hash the password if it has been modified (or is new)
    if (!field.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(field.password, salt, async (err, hash) => {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            field.password = hash;
            //Autoincremental id.
            if(field.isNew){
                try {
                    field.field_id = await getIncrementalID();
                }
                catch(err) {
                    next(err);
                }
            }
            next();
        });
    });
});

FieldSchema.methods.comparePassword = async function(candidatePassword) {
    return await bcrypt.compare(candidatePassword, this.password);
};

async function getIncrementalID(){
    let counterDoc;

    try {
        counterDoc = await Counter.findByIdAndUpdate({_id: 'field_id'}, {$inc: { seq: 1} }).exec();
    }
    catch(err){
        return err;
    }
    return counterDoc.seq;
}

//initialize();
function initialize(){
    let fieldCounter = new Counter({
        _id: 'field_id'
    });
    fieldCounter.save();
}

export default mongoose.model('Field', FieldSchema);