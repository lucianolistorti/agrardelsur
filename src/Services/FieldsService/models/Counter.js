import mongoose from 'mongoose';

//CounterSchema for autoincremental ids. Initialize only once.
let CounterSchema = new mongoose.Schema({
    _id: { type: String, required: true},
    seq: { type: Number, default: 0 }
});

export default mongoose.model('Counter', CounterSchema);