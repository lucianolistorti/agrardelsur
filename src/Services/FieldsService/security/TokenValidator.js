import jwt from 'jsonwebtoken';
import config from '../config';

export const VerifyToken = async function (req,res,next){
    var token = req.headers['authorization'];

    if(!token){
        return res.status(401).send({
            custom_message: "Es necesaria la autenticación."
        });
    }
    token = token.replace('Bearer ', '')
    let tokenData;

    try {
        tokenData = await jwt.verify(token, config.jwt.key);
        req.userId = tokenData.userId;
        req.role = tokenData.role;
    }
    catch(err) {
        return res.status(401).send({ 
            custom_message: 'Autenticación inválida.',
            debug_error: err.message 
        });
    }
    next();
}

export const VerifyAdminToken = async function (req,res,next){
    var token = req.headers['authorization'];

    if(!token){
        return res.status(401).send({
            custom_message: "Es necesaria la autenticación."
        });
    }
    token = token.replace('Bearer ', '')
    let tokenData;

    try {
        tokenData = await jwt.verify(token, config.jwt.key);
        req.userId = tokenData.userId;
        req.role = tokenData.role;
        if(tokenData.role !== 1) //1 = Admin
            throw new Error(`Invalid permissions.`)
    }
    catch(err) {
        return res.status(401).send({ 
            custom_message: 'Autenticación inválida.',
            debug_error: err.message 
        });
    }
    next();
}