import mongoose from 'mongoose';
import config from './config';

mongoose.connect(config.db_connection, { useNewUrlParser: true , useCreateIndex: true }, (err) => {
    if(err)
        console.log(`Could not connect to Database ${err}`);
    console.log('Successfully connected to Database.')
});