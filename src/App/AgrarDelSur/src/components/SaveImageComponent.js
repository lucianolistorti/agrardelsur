import React, { Component } from 'react';
import { Text, 
        Dimensions, 
        TouchableOpacity, 
        AppRegistry, 
        View, 
        ImageBackground, 
        StyleSheet, 
        CameraRoll 
} from 'react-native';

export default class SaveImageComponent extends Component {

    saveImage = function () {
        console.log(`Saving image... ${this.props.source.uri}`);
        CameraRoll.saveToCameraRoll(this.props.source.uri, 'photo');
    }
    
    cancelImage = function () {
        console.log('Canceling image...');
        this.props.cancelImage();
    }

    render() {
        return (
            <View>
                <ImageBackground
                    source={{ uri: this.props.source.uri }}
                    style={styles.fullSize}
                />
                <TouchableOpacity
                    onPress={this.saveImage.bind(this)}
                    style={styles.imageButton}
                >
                    <Text style={{ fontSize: 14 }}> SAVE </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={this.cancelImage.bind(this)}
                    style={styles.imageButton}
                >
                    <Text style={{ fontSize: 14 }}> CANCEL</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fullSize: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        position: 'absolute'
    },
    imageButton: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
      }
});

AppRegistry.registerComponent('SaveImageComponent', () => SaveImageComponent);
