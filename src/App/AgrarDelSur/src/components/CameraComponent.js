'use strict';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import SaveImageComponent from './SaveImageComponent';

export default class CameraComponent extends Component {

    constructor (props) {
        super(props);
        this.state = {
            showImage: false
        };
        this.cancelImage = this.cancelImage.bind(this);
    } 

    render() {
        if(this.state.showImage){
            return this.renderImage(); 
        } 
        else {
            return (
            <View style={styles.container}>
                <RNCamera
                    ref={ ref => {this.camera = ref; }}
                    style = {styles.preview}
                    permissionDialogTitle={'Permission to use camera'}
                    permissionDialogMessage={'We need your permission to use your camera phone'}
                />
                <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center',}}>
                <TouchableOpacity
                    onPress={this.takePicture.bind(this)}
                    style = {styles.imageButton}
                >
                    <Text style={{fontSize: 14}}> SNAP </Text>
                </TouchableOpacity>

                </View>
            </View>
            );
        }
    }

    cancelImage() {
        console.log('cancelImage');
        this.setState({
            showImage:false
        });
    }

    showImage = () => {
        console.log(`Showing image... ${this.data.uri}`);
        this.setState({showImage:true});
    }

    renderImage = function () {
        console.log(`Rendering image... ${this.data.uri}`);
        return (
            <View style={styles.imageContainer}>
                <SaveImageComponent source={this.data} cancelImage={this.cancelImage}/>
            </View>
        );
    }

    takePicture = async function() {
        if (this.camera) {
            const options = {
                quality: 1,
                orientation: "portrait",
                pauseAfterCapture: true,
                fixOrientation: true
            }
            this.data = await this.camera.takePictureAsync(options);
            console.log('Taking picture... ' + this.data.uri);
            this.showImage();
        }
    };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  imageButton: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20
  }
});

AppRegistry.registerComponent('CameraComponent', () => CameraComponent);